package com.appointment.pay.service;

/**
 * Rabbit MQ 消息接口
 * @author zhul
 *
 */
public interface IMessageService {
    
    /**
     * 默认发送消息接口
     * @param object 消息内容
     */
    void send(Object object);
    
    /**
     * 指定exchange,routingKey发送消息
     * @param exchange
     * @param routingKey
     * @param object 消息内容
     */
    void send(String exchange, String routingKey, Object object);
    
}
