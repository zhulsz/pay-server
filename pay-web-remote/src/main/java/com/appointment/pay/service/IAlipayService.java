package com.appointment.pay.service;

import com.appointment.pay.vo.AlipayRequestVo;
import com.appointment.pay.vo.AlipayResponseVo;

/**
 * 支付宝支付dubbo接口
 * @author zhul
 *
 */
public interface IAlipayService {
    
    /**
     * 预下单接口
     * @param request
     * @return
     */
    AlipayResponseVo precreate(AlipayRequestVo request);
    
}
