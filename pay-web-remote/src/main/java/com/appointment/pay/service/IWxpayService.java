package com.appointment.pay.service;

import com.appointment.pay.vo.WxpayRequestVo;
import com.appointment.pay.vo.WxpayResponseVo;
import com.appointment.pay.vo.WxpayTransactionVo;

/**
 * 微信支付dubbo接口
 * @author zhul
 *
 */
public interface IWxpayService {
    
    /**
     * 支付下单接口
     * @param request
     * @return
     */
    public WxpayResponseVo unifiedorder(WxpayRequestVo request);
    
    /**
     * 根据预订单号查询订单
     * @param prepayid
     * @return
     */
    public WxpayTransactionVo queryOrder(String prepayid);
}
