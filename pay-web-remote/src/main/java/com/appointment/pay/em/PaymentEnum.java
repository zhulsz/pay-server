package com.appointment.pay.em;

/**
 * Created by zhul on 2016/5/5.
 */
public enum PaymentEnum {
    UNPAY(0, "未支付"), HASPAY(1, "已支付"), REFUND(2, "转入退款"),ERROR(3,"支付失败"),REVERSE(4,"已撤销"),CLOSED(5,"已关闭");


    private int state;
    private String name;

    PaymentEnum(int state, String name) {
        this.state = state;
        this.name = name;
    }

    public int getState() {
        return state;
    }

    public void setState(int state) {
        this.state = state;
    }

   public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public static PaymentEnum getPaymentEnum(int state){
        for(PaymentEnum paymentEnum:PaymentEnum.values()){
            if(paymentEnum.getState()==state){
                return paymentEnum;
            }
        }
        return null;
    }
}
