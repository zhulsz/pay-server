package com.appointment.pay.exception;

/**
 * <b>类描述：</b><br/>
 * <b>创建人：</b><br/>
 * <b>修改人：</b><br/>
 * <b>修改时间：</b><br/>
 * <b>修改备注：</b><br/>
 */
public class PayRemoteException extends RuntimeException {
    /**
	 * 
	 */
	private static final long serialVersionUID = -7588722588315987845L;
	private String errorCode = "";

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public PayRemoteException(String message, String errorCode) {
        super(errorCode+message);
        this.errorCode = errorCode;
    }

    public PayRemoteException(String message, Throwable cause, String errorCode) {
        super(message, cause);
        this.errorCode = errorCode;
    }

    public PayRemoteException(Throwable cause, String errorCode) {
        super(cause);
        this.errorCode = errorCode;
    }

    public PayRemoteException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace, String errorCode) {
        //super(message, cause, enableSuppression, writableStackTrace);
        this.errorCode = errorCode;
    }

    public PayRemoteException(String message) {
        super(message);
    }

    public PayRemoteException(String message, Throwable cause) {
        super(message, cause);
    }

    public PayRemoteException(Throwable cause) {
        super(cause);
    }

    public PayRemoteException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
       // super(message, cause, enableSuppression, writableStackTrace);
    }
}
