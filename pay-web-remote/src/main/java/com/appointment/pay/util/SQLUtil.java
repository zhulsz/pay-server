package com.appointment.pay.util;

public class SQLUtil {
    /**
     * 处理用于SQL查询的字符串，将特殊字符转换
     *
     * @param paramStr :原字符串
     * @return 转换后的字符串
     * @author Ban
     * @mail evan.ban@suixingpay.com
     * @date 2011-7-12 下午02:54:30
     */
    public static String dealSQLStr(String paramStr) {
        if (paramStr == null || "".equals(paramStr.trim()))
            return "%";
        StringBuilder sb = new StringBuilder();
        sb.append("%");
        for (int i = 0; i < paramStr.length(); i++) {
            char ch = paramStr.charAt(i);
            if (ch == '[' || ch == ']' || ch == '%' || ch == '^' || ch == '_')
                sb.append("[" + ch + "]");
            else
                sb.append(ch);
        }
        sb.append("%");
        return sb.toString();
    }
}
