package com.appointment.pay.param;

import java.io.Serializable;

import com.appointment.pay.vo.PageVo;

/**
 * Created by Carmel on 2014/9/25.
 */
public abstract class PageSerch implements Serializable {
	/**
     * 
     */
    private static final long serialVersionUID = -9208617028263234750L;

    /**
	 * 每页数量
	 */
	protected int pageSize=10;

	/**
	 * 页码
	 */
	protected int pageCount;

	public int getPageSize() {
		return pageSize;
	}

	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}

	public int getPageCount() {
		return pageCount;
	}

	public void setPageCount(int pageCount) {
		this.pageCount = pageCount;
	}

	protected abstract String getOrderBy();

	public PageVo getPage() {
		PageVo page = new PageVo();
		page.setPageSize(pageSize);
		page.setPageCount(pageCount);
		page.setOrderBy(getOrderBy());
		return page;
	}
}
