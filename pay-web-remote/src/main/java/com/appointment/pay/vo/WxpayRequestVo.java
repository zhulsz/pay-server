package com.appointment.pay.vo;

import java.io.Serializable;

/**
 * 微信支付dubbo请求参数
 * @author zhul
 *
 */
public class WxpayRequestVo implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 1299314914294452560L;
    
    /**
     * 应用ID
     */
    private String appid;
    
    /**
     * 商户号
     */
    private String mch_id;
    
    /**
     * 设备号
     */
    private String device_info;
    
    /**
     * 商品描述
     */
    private String body;
        
    /**
     * 总金额,单位为分
     */
    private Integer total_fee;
    
    /**
     * 交易类型,APP
     */
    private String trade_type;
    
    /**
     * 商户密钥
     */
    private String key;
    
    /**
     * 用户id
     */
    private Integer userId;
    
    /**
     * openid
     */
    private String openid;

    public String getOpenid() {
        return openid;
    }

    public void setOpenid(String openid) {
        this.openid = openid;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getAppid() {
        return appid;
    }

    public void setAppid(String appid) {
        this.appid = appid;
    }

    public String getMch_id() {
        return mch_id;
    }

    public void setMch_id(String mch_id) {
        this.mch_id = mch_id;
    }

    public String getDevice_info() {
        return device_info;
    }

    public void setDevice_info(String device_info) {
        this.device_info = device_info;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public Integer getTotal_fee() {
        return total_fee;
    }

    public void setTotal_fee(Integer total_fee) {
        this.total_fee = total_fee;
    }

    public String getTrade_type() {
        return trade_type;
    }

    public void setTrade_type(String trade_type) {
        this.trade_type = trade_type;
    }

}
