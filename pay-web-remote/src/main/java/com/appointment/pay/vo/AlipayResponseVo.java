package com.appointment.pay.vo;

import java.io.Serializable;

/**
 * 支付宝预下单接口返回对象
 * @author zhul
 *
 */
public class AlipayResponseVo implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -529189451368641825L;
    
    /**
     * 商户订单号
     */
    private String out_trade_no;
    
    /**
     * 二维码码串
     */
    private String qr_code;

    public String getOut_trade_no() {
        return out_trade_no;
    }

    public void setOut_trade_no(String out_trade_no) {
        this.out_trade_no = out_trade_no;
    }

    public String getQr_code() {
        return qr_code;
    }

    public void setQr_code(String qr_code) {
        this.qr_code = qr_code;
    }
    
}
