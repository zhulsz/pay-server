package com.appointment.pay.vo;

import java.io.Serializable;

/**
 * 微信支付订单接口返回对象
 * @author zhul
 *
 */
public class WxpayTransactionVo implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 1312761482849188951L;
    
    /**
     *  微信支付订单号
     */
    private String transactionId;
    /**
     *  商户订单号
     */
    private String outTradeNo;
    
    /**
     *  总金额
     */
    private Integer totalFee;
    
    /**
     *  支付完成时间
     */
    private Long timeEnd;
    
    /**
     *  交易状态：SUCCESS—支付成功,REFUND—转入退款,NOTPAY—未支付,CLOSED—已关闭,REVERSE—已冲正,REVOK—已撤销
     */
    private String tradeState;
    
    /**
     *  支付状态
     */
    private String stateName;

    public String getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }

    public String getOutTradeNo() {
        return outTradeNo;
    }

    public void setOutTradeNo(String outTradeNo) {
        this.outTradeNo = outTradeNo;
    }

    public Integer getTotalFee() {
        return totalFee;
    }

    public void setTotalFee(Integer totalFee) {
        this.totalFee = totalFee;
    }

    public Long getTimeEnd() {
        return timeEnd;
    }

    public void setTimeEnd(Long timeEnd) {
        this.timeEnd = timeEnd;
    }

    public String getTradeState() {
        return tradeState;
    }

    public void setTradeState(String tradeState) {
        this.tradeState = tradeState;
    }

    public String getStateName() {
        return stateName;
    }

    public void setStateName(String stateName) {
        this.stateName = stateName;
    }
    
}
