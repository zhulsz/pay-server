package com.appointment.pay.vo;

import java.io.Serializable;

/**
 * 支付宝支付请求参数对象
 * @author zhul
 *
 */
public class AlipayRequestVo implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -7251557176150137687L;
    
    /**
     * 开发者的AppId
     */
    private String app_id;
    
    /**
     * 业务参数,JSON 格式
     */
    private String biz_content;
    
    /**
     * 用户id
     */
    private Integer userId;

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getApp_id() {
        return app_id;
    }

    public void setApp_id(String app_id) {
        this.app_id = app_id;
    }

    public String getBiz_content() {
        return biz_content;
    }

    public void setBiz_content(String biz_content) {
        this.biz_content = biz_content;
    }
    
}
