package com.appointment.pay.vo;

import java.io.Serializable;
import java.util.List;

/**
 * Created by Carmel on 2014/9/26.
 */
public class PageResultVo<T>  implements Serializable {

	private PageVo page;

	private List<T> resultList ;

	public PageVo getPage() {
		return page;
	}

	public void setPage(PageVo page) {
		this.page = page;
	}

	public List<T> getResultList() {
		return resultList;
	}

	public void setResultList(List<T> resultList) {
		this.resultList = resultList;
	}
}