package com.appointment.pay.vo;

import java.io.Serializable;
import java.util.List;

/**
 * Created by serv on 2014/9/9.
 */
public class PageResult implements Serializable{

    private long total;
    private int pageNum;
    private int pageSize;
    private List content;

    public PageResult(long total, int pageNum, int pageSize, List content) {
        this.total = total;
        this.pageNum = pageNum;
        this.pageSize = pageSize;
        this.content = content;
    }

    public PageResult(List content) {
        this.content = content;
    }

    /**
     * Returns the number of total pages.
     *
     * @return the number of toral pages
     */
    public int getTotalPages(){
        return pageSize == 0 ? 1 : (int) Math.ceil((double) total / (double) pageSize);
    }

    /**
     * Returns the total amount of elements.
     *
     * @return the total amount of elements
     */
    public long getTotalElements(){
        return total;
    }

    public long getTotal() {
        return total;
    }

    public void setTotal(long total) {
        this.total = total;
    }

    public int getPageNum() {
        return pageNum;
    }

    public void setPageNum(int pageNum) {
        this.pageNum = pageNum;
    }

    public int getPageSize() {
        return pageSize;
    }

    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }

    public List getContent() {
        return content;
    }

    public void setContent(List content) {
        this.content = content;
    }
}
