package com.appointment.pay.vo;

import java.io.Serializable;

/**
 * Created by Carmel on 2014/9/26.
 */
public class PageVo  implements Serializable {
	private int count;
	private int pageSize;
	private int pageCount;
	private String orderBy;

	public int getCount() {
		return count;
	}

	public void setCount(int count) {
		this.count = count;
	}

	public int getPageSize() {
		return pageSize;
	}

	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}

	public int getPageCount() {
		return pageCount;
	}

	public void setPageCount(int pageCount) {
		this.pageCount = pageCount;
	}

	public String getOrderBy() {
		return orderBy;
	}

	public void setOrderBy(String orderBy) {
		this.orderBy = orderBy;
	}
}
