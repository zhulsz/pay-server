package com.appointment.pay.core.enums;


/**
 * 交易状态
 * @author zhul
 *
 */
public enum TradeStateEnum {
    
    SUCCESS("SUCCESS","支付成功"),
    
    REFUND("REFUND","转入退款"),
    
    NOTPAY("NOTPAY","未支付"),
    
    CLOSED("CLOSED","已关闭"),
    
    FAIL("FAIL","支付失败"),
    
    REVOK("REVOK","已撤销");
    
    private String code;
    
    private String message;
    
    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    private TradeStateEnum(String code,String message){
        this.code = code;
        this.message = message;
    }
    
    public static String getMsg(String code) {
        for (TradeStateEnum stateEnum : values()) {
            if(stateEnum.getCode().equalsIgnoreCase(code)){
                return stateEnum.getMessage();
            }
        }
        return null;
    }
    
}
