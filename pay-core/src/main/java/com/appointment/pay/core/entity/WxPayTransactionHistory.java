package com.appointment.pay.core.entity;

import java.io.Serializable;
import java.util.Date;

/**
 * 微信交易对象
 * @author zhul
 *
 */
public class WxPayTransactionHistory implements Serializable {
    
    /**
     * 
     */
    private static final long serialVersionUID = -1969703092275243136L;

    /**
     *  
     */
    private Integer id;
    /**
     *  商家id
     */
    private String mchId;
    /**
     *  子商家id
     */
    private String subMchId;
    /**
     *  微信公众账号id
     */
    private String appId;
    /**
     *  设备号
     */
    private String deviceInfo;
    /**
     *  微信用户标识即微信中的openid
     */
    private String openId;
    /**
     *  子openId
     */
    private String subOpenId;
    /**
     *  n未关注  y关注
     */
    private String isSubscribe;
    /**
     *  交易类型 jsapi、native、micropay、 app
     */
    private String tradeType;
    /**
     *  主平台商家id
     */
    private String shopId;
    /**
     *  门店ID
     */
    private String storeId;
    /**
     *  门店名称
     */
    private String storeName;
    /**
     *  付款银行
     */
    private String bankType;
    /**
     *  微信支付订单号
     */
    private String transactionId;
    /**
     *  商户订单号
     */
    private String outTradeNo;
    /**
     *  用户id
     */
    private Integer userId;
    /**
     *  总金额
     */
    private Integer totalFee;
    /**
     *  现金券金额
     */
    private Integer couponFee;
    /**
     *  货币类型
     */
    private String feeType;
    /**
     *  商品描述
     */
    private String body;
    /**
     *  附加数据
     */
    private String attach;
    /**
     *  终端ip
     */
    private String spbillCreateIp;
    /**
     *  
     */
    private Long timeStart;
    /**
     *  交易过期时间
     */
    private Long timeExpire;
    /**
     *  商品标记
     */
    private String goodsTag;
    /**
     *  商品id
     */
    private String productId;
    /**
     *  预支付id
     */
    private String prepayId;
    /**
     *  支付链接
     */
    private String codeUrl;
    /**
     *  支付完成时间
     */
    private Long timeEnd;
    /**
     *  时间戳
     */
    private Long systemTimeSign;
    /**
     *  0 未支付 1已支付 2已退款
     */
    private Integer state;
    /**
     *  支付状态
     */
    private String stateName;
    /**
     *  错误原因
     */
    private String errorMsg;
    /**
     *  业务系统id
     */
    private String businessSystemId;
    /**
     *  pos每次请求唯一标识
     */
    private String channelId;
    /**
     *  交易状态：SUCCESS—支付成功,REFUND—转入退款,NOTPAY—未支付,CLOSED—已关闭,REVERSE—已冲正,REVOK—已撤销
     */
    private String tradeState;
    /**
     *  设备通知标识
     */
    private String partner;
    /**
     *  授权码
     */
    private String authCode;
    /**
     *  实收金额
     */
    private Integer cashFee;
    /**
     *  子商户号n未关注  y关注
     */
    private String subIsSubscribe;
    /**
     *  
     */
    private java.util.Date createTime;
    /**
     *  状态更正状态时间
     */
    private java.util.Date correctTime;
    /**
     * 
     * @param id
     */
    public void setId(Integer id){
        this.id = id;
    }
    
    /**
     * 
     * @return
     */ 
    public Integer getId(){
        return id;
    }
    /**
     * 商家id
     * @param mchId
     */
    public void setMchId(String mchId){
        this.mchId = mchId;
    }
    
    /**
     * 商家id
     * @return
     */ 
    public String getMchId(){
        return mchId;
    }
    /**
     * 子商家id
     * @param subMchId
     */
    public void setSubMchId(String subMchId){
        this.subMchId = subMchId;
    }
    
    /**
     * 子商家id
     * @return
     */ 
    public String getSubMchId(){
        return subMchId;
    }
    /**
     * 微信公众账号id
     * @param appId
     */
    public void setAppId(String appId){
        this.appId = appId;
    }
    
    /**
     * 微信公众账号id
     * @return
     */ 
    public String getAppId(){
        return appId;
    }
    /**
     * 设备号
     * @param deviceInfo
     */
    public void setDeviceInfo(String deviceInfo){
        this.deviceInfo = deviceInfo;
    }
    
    /**
     * 设备号
     * @return
     */ 
    public String getDeviceInfo(){
        return deviceInfo;
    }
    /**
     * 微信用户标识即微信中的openid
     * @param openId
     */
    public void setOpenId(String openId){
        this.openId = openId;
    }
    
    /**
     * 微信用户标识即微信中的openid
     * @return
     */ 
    public String getOpenId(){
        return openId;
    }
    /**
     * 子openId
     * @param subOpenId
     */
    public void setSubOpenId(String subOpenId){
        this.subOpenId = subOpenId;
    }
    
    /**
     * 子openId
     * @return
     */ 
    public String getSubOpenId(){
        return subOpenId;
    }
    /**
     * n未关注  y关注
     * @param isSubscribe
     */
    public void setIsSubscribe(String isSubscribe){
        this.isSubscribe = isSubscribe;
    }
    
    /**
     * n未关注  y关注
     * @return
     */ 
    public String getIsSubscribe(){
        return isSubscribe;
    }
    /**
     * 交易类型 jsapi、native、micropay、 app
     * @param tradeType
     */
    public void setTradeType(String tradeType){
        this.tradeType = tradeType;
    }
    
    /**
     * 交易类型 jsapi、native、micropay、 app
     * @return
     */ 
    public String getTradeType(){
        return tradeType;
    }
    /**
     * 主平台商家id
     * @param shopId
     */
    public void setShopId(String shopId){
        this.shopId = shopId;
    }
    
    /**
     * 主平台商家id
     * @return
     */ 
    public String getShopId(){
        return shopId;
    }
    /**
     * 门店ID
     * @param storeId
     */
    public void setStoreId(String storeId){
        this.storeId = storeId;
    }
    
    /**
     * 门店ID
     * @return
     */ 
    public String getStoreId(){
        return storeId;
    }
    /**
     * 门店名称
     * @param storeName
     */
    public void setStoreName(String storeName){
        this.storeName = storeName;
    }
    
    /**
     * 门店名称
     * @return
     */ 
    public String getStoreName(){
        return storeName;
    }
    /**
     * 付款银行
     * @param bankType
     */
    public void setBankType(String bankType){
        this.bankType = bankType;
    }
    
    /**
     * 付款银行
     * @return
     */ 
    public String getBankType(){
        return bankType;
    }
    /**
     * 微信支付订单号
     * @param transactionId
     */
    public void setTransactionId(String transactionId){
        this.transactionId = transactionId;
    }
    
    /**
     * 微信支付订单号
     * @return
     */ 
    public String getTransactionId(){
        return transactionId;
    }
    /**
     * 商户订单号
     * @param outTradeNo
     */
    public void setOutTradeNo(String outTradeNo){
        this.outTradeNo = outTradeNo;
    }
    
    /**
     * 商户订单号
     * @return
     */ 
    public String getOutTradeNo(){
        return outTradeNo;
    }
    /**
     * 用户id
     * @param userId
     */
    public void setUserId(Integer userId){
        this.userId = userId;
    }
    
    /**
     * 用户id
     * @return
     */ 
    public Integer getUserId(){
        return userId;
    }
    /**
     * 总金额
     * @param totalFee
     */
    public void setTotalFee(Integer totalFee){
        this.totalFee = totalFee;
    }
    
    /**
     * 总金额
     * @return
     */ 
    public Integer getTotalFee(){
        return totalFee;
    }
    /**
     * 现金券金额
     * @param couponFee
     */
    public void setCouponFee(Integer couponFee){
        this.couponFee = couponFee;
    }
    
    /**
     * 现金券金额
     * @return
     */ 
    public Integer getCouponFee(){
        return couponFee;
    }
    /**
     * 货币类型
     * @param feeType
     */
    public void setFeeType(String feeType){
        this.feeType = feeType;
    }
    
    /**
     * 货币类型
     * @return
     */ 
    public String getFeeType(){
        return feeType;
    }
    /**
     * 商品描述
     * @param body
     */
    public void setBody(String body){
        this.body = body;
    }
    
    /**
     * 商品描述
     * @return
     */ 
    public String getBody(){
        return body;
    }
    /**
     * 附加数据
     * @param attach
     */
    public void setAttach(String attach){
        this.attach = attach;
    }
    
    /**
     * 附加数据
     * @return
     */ 
    public String getAttach(){
        return attach;
    }
    /**
     * 终端ip
     * @param spbillCreateIp
     */
    public void setSpbillCreateIp(String spbillCreateIp){
        this.spbillCreateIp = spbillCreateIp;
    }
    
    /**
     * 终端ip
     * @return
     */ 
    public String getSpbillCreateIp(){
        return spbillCreateIp;
    }
    /**
     * 
     * @param timeStart
     */
    public void setTimeStart(Long timeStart){
        this.timeStart = timeStart;
    }
    
    /**
     * 
     * @return
     */ 
    public Long getTimeStart(){
        return timeStart;
    }
    /**
     * 交易过期时间
     * @param timeExpire
     */
    public void setTimeExpire(Long timeExpire){
        this.timeExpire = timeExpire;
    }
    
    /**
     * 交易过期时间
     * @return
     */ 
    public Long getTimeExpire(){
        return timeExpire;
    }
    /**
     * 商品标记
     * @param goodsTag
     */
    public void setGoodsTag(String goodsTag){
        this.goodsTag = goodsTag;
    }
    
    /**
     * 商品标记
     * @return
     */ 
    public String getGoodsTag(){
        return goodsTag;
    }
    /**
     * 商品id
     * @param productId
     */
    public void setProductId(String productId){
        this.productId = productId;
    }
    
    /**
     * 商品id
     * @return
     */ 
    public String getProductId(){
        return productId;
    }
    /**
     * 预支付id
     * @param prepayId
     */
    public void setPrepayId(String prepayId){
        this.prepayId = prepayId;
    }
    
    /**
     * 预支付id
     * @return
     */ 
    public String getPrepayId(){
        return prepayId;
    }
    /**
     * 支付链接
     * @param codeUrl
     */
    public void setCodeUrl(String codeUrl){
        this.codeUrl = codeUrl;
    }
    
    /**
     * 支付链接
     * @return
     */ 
    public String getCodeUrl(){
        return codeUrl;
    }
    /**
     * 支付完成时间
     * @param timeEnd
     */
    public void setTimeEnd(Long timeEnd){
        this.timeEnd = timeEnd;
    }
    
    /**
     * 支付完成时间
     * @return
     */ 
    public Long getTimeEnd(){
        return timeEnd;
    }
    /**
     * 时间戳
     * @param systemTimeSign
     */
    public void setSystemTimeSign(Long systemTimeSign){
        this.systemTimeSign = systemTimeSign;
    }
    
    /**
     * 时间戳
     * @return
     */ 
    public Long getSystemTimeSign(){
        return systemTimeSign;
    }
    /**
     * 0 未支付 1已支付 2已退款
     * @param state
     */
    public void setState(Integer state){
        this.state = state;
    }
    
    /**
     * 0 未支付 1已支付 2已退款
     * @return
     */ 
    public Integer getState(){
        return state;
    }
    /**
     * 支付状态
     * @param stateName
     */
    public void setStateName(String stateName){
        this.stateName = stateName;
    }
    
    /**
     * 支付状态
     * @return
     */ 
    public String getStateName(){
        return stateName;
    }
    /**
     * 错误原因
     * @param errorMsg
     */
    public void setErrorMsg(String errorMsg){
        this.errorMsg = errorMsg;
    }
    
    /**
     * 错误原因
     * @return
     */ 
    public String getErrorMsg(){
        return errorMsg;
    }
    /**
     * 业务系统id
     * @param businessSystemId
     */
    public void setBusinessSystemId(String businessSystemId){
        this.businessSystemId = businessSystemId;
    }
    
    /**
     * 业务系统id
     * @return
     */ 
    public String getBusinessSystemId(){
        return businessSystemId;
    }
    /**
     * pos每次请求唯一标识
     * @param channelId
     */
    public void setChannelId(String channelId){
        this.channelId = channelId;
    }
    
    /**
     * pos每次请求唯一标识
     * @return
     */ 
    public String getChannelId(){
        return channelId;
    }
    /**
     * 交易状态：SUCCESS—支付成功,REFUND—转入退款,NOTPAY—未支付,CLOSED—已关闭,REVERSE—已冲正,REVOK—已撤销
     * @param tradeState
     */
    public void setTradeState(String tradeState){
        this.tradeState = tradeState;
    }
    
    /**
     * 交易状态：SUCCESS—支付成功,REFUND—转入退款,NOTPAY—未支付,CLOSED—已关闭,REVERSE—已冲正,REVOK—已撤销
     * @return
     */ 
    public String getTradeState(){
        return tradeState;
    }
    /**
     * 设备通知标识
     * @param partner
     */
    public void setPartner(String partner){
        this.partner = partner;
    }
    
    /**
     * 设备通知标识
     * @return
     */ 
    public String getPartner(){
        return partner;
    }
    /**
     * 授权码
     * @param authCode
     */
    public void setAuthCode(String authCode){
        this.authCode = authCode;
    }
    
    /**
     * 授权码
     * @return
     */ 
    public String getAuthCode(){
        return authCode;
    }
    /**
     * 实收金额
     * @param cashFee
     */
    public void setCashFee(Integer cashFee){
        this.cashFee = cashFee;
    }
    
    /**
     * 实收金额
     * @return
     */ 
    public Integer getCashFee(){
        return cashFee;
    }
    /**
     * 子商户号n未关注  y关注
     * @param subIsSubscribe
     */
    public void setSubIsSubscribe(String subIsSubscribe){
        this.subIsSubscribe = subIsSubscribe;
    }
    
    /**
     * 子商户号n未关注  y关注
     * @return
     */ 
    public String getSubIsSubscribe(){
        return subIsSubscribe;
    }
    /**
     * 
     * @param createTime
     */
    public void setCreateTime(java.util.Date createTime){
        this.createTime = createTime;
    }
    
    /**
     * 
     * @return
     */ 
    public java.util.Date getCreateTime(){
        return createTime;
    }
    /**
     * 状态更正状态时间
     * @param correctTime
     */
    public void setCorrectTime(java.util.Date correctTime){
        this.correctTime = correctTime;
    }
    
    /**
     * 状态更正状态时间
     * @return
     */ 
    public java.util.Date getCorrectTime(){
        return correctTime;
    }
}