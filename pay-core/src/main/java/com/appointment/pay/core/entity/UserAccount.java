package com.appointment.pay.core.entity;

import java.io.Serializable;

/**
 * 用户账户对象
 * @author zhul
 */
public class UserAccount implements Serializable {
	/**
     * 
     */
    private static final long serialVersionUID = 620938200425971899L;
    
    /**
	 *  账户id
	 */
	private Integer id;
	/**
	 *  用户id
	 */
	private Integer userId;
	/**
	 *  账户余额
	 */
	private java.math.BigDecimal amount;
	/**
	 *  累积收入
	 */
	private java.math.BigDecimal accumulatedIncome;
	/**
	 *  累积消费
	 */
	private java.math.BigDecimal accumulatedConsum;
	/**
	 *  累积提现
	 */
	private java.math.BigDecimal totalWithdraw;
	/**
	 *  累积充值
	 */
	private java.math.BigDecimal totalRecharge;
	/**
	 *  即将到账
	 */
	private java.math.BigDecimal comingAccount;
	/**
	 *  创建时间
	 */
	private java.util.Date createTime;
	/**
	 *  修改时间
	 */
	private java.util.Date updateTime;
	/**
	 * 账户id
	 * @param id
	 */
	public void setId(Integer id){
		this.id = id;
	}
	
    /**
     * 账户id
     * @return
     */	
    public Integer getId(){
    	return id;
    }
	/**
	 * 用户id
	 * @param userId
	 */
	public void setUserId(Integer userId){
		this.userId = userId;
	}
	
    /**
     * 用户id
     * @return
     */	
    public Integer getUserId(){
    	return userId;
    }
	/**
	 * 账户余额
	 * @param amount
	 */
	public void setAmount(java.math.BigDecimal amount){
		this.amount = amount;
	}
	
    /**
     * 账户余额
     * @return
     */	
    public java.math.BigDecimal getAmount(){
    	return amount;
    }
	/**
	 * 累积收入
	 * @param accumulatedIncome
	 */
	public void setAccumulatedIncome(java.math.BigDecimal accumulatedIncome){
		this.accumulatedIncome = accumulatedIncome;
	}
	
    /**
     * 累积收入
     * @return
     */	
    public java.math.BigDecimal getAccumulatedIncome(){
    	return accumulatedIncome;
    }
	/**
	 * 累积消费
	 * @param accumulatedConsum
	 */
	public void setAccumulatedConsum(java.math.BigDecimal accumulatedConsum){
		this.accumulatedConsum = accumulatedConsum;
	}
	
    /**
     * 累积消费
     * @return
     */	
    public java.math.BigDecimal getAccumulatedConsum(){
    	return accumulatedConsum;
    }
	/**
	 * 累积提现
	 * @param totalWithdraw
	 */
	public void setTotalWithdraw(java.math.BigDecimal totalWithdraw){
		this.totalWithdraw = totalWithdraw;
	}
	
    /**
     * 累积提现
     * @return
     */	
    public java.math.BigDecimal getTotalWithdraw(){
    	return totalWithdraw;
    }
	/**
	 * 累积充值
	 * @param totalRecharge
	 */
	public void setTotalRecharge(java.math.BigDecimal totalRecharge){
		this.totalRecharge = totalRecharge;
	}
	
    /**
     * 累积充值
     * @return
     */	
    public java.math.BigDecimal getTotalRecharge(){
    	return totalRecharge;
    }
	/**
	 * 即将到账
	 * @param comingAccount
	 */
	public void setComingAccount(java.math.BigDecimal comingAccount){
		this.comingAccount = comingAccount;
	}
	
    /**
     * 即将到账
     * @return
     */	
    public java.math.BigDecimal getComingAccount(){
    	return comingAccount;
    }
	/**
	 * 创建时间
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime){
		this.createTime = createTime;
	}
	
    /**
     * 创建时间
     * @return
     */	
    public java.util.Date getCreateTime(){
    	return createTime;
    }
	/**
	 * 修改时间
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime){
		this.updateTime = updateTime;
	}
	
    /**
     * 修改时间
     * @return
     */	
    public java.util.Date getUpdateTime(){
    	return updateTime;
    }
}