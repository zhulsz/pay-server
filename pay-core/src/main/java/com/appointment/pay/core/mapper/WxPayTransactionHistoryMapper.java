package com.appointment.pay.core.mapper;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;

import com.appointment.pay.core.entity.WxPayTransactionHistory;
import com.hyxt.platform.commons.page.Page;
import com.hyxt.platform.commons.page.Pageable;

public interface WxPayTransactionHistoryMapper {

    void insertWxPayTransactionHistory(WxPayTransactionHistory wxPayTransactionHistory);

    void deleteWxPayTransactionHistoryById(Integer id);

    void updateWxPayTransactionHistory(WxPayTransactionHistory wxPayTransactionHistory);
    
    void updateByOutTradeNo(WxPayTransactionHistory wxpayTransaction);

    Page<WxPayTransactionHistory> searchWxPayTransactionHistoryByParams(@Param("map")Map<String, String> map , Pageable pageable);

    List<WxPayTransactionHistory> searchWxPayTransactionHistoryByParams(@Param("map")Map<String, Object> map);
    
    WxPayTransactionHistory queryByOutTradeNo(@Param("outTradeNo") String outTradeNo);
    
    WxPayTransactionHistory queryByPrepayid(@Param("prepayId") String prepayid);

}