package com.appointment.pay.core.entity;

/**
 * 支付宝订单对象
 * @author zhul
 */
public class OrderInfo {
	/**
	 *  
	 */
	private Integer id;
	/**
	 *  用户id
	 */
	private Integer userId;
	/**
	 *  支付宝接口名称
	 */
	private String method;
	/**
	 *  支付宝交易凭证号
	 */
	private String tradeNo;
	/**
	 *  商户订单号
	 */
	private String outTradeNo;
	/**
	 *  订单金额
	 */
	private java.math.BigDecimal totalAmount;
	/**
	 *  可打折金额
	 */
	private java.math.BigDecimal discountableAmount;
	/**
	 *  不可打折金额
	 */
	private java.math.BigDecimal undiscountableAmount;
	/**
	 *  订单标题
	 */
	private String subject;
	/**
	 *  订单描述
	 */
	private String body;
	/**
	 *  商品编号
	 */
	private String goodsId;
	/**
	 *  支付宝统一的商品编号
	 */
	private String alipayGoodsId;
	/**
	 *  商品名称
	 */
	private String goodsName;
	/**
	 *  商品数量
	 */
	private String quantity;
	/**
	 *  商品单价
	 */
	private java.math.BigDecimal price;
	/**
	 *  商品类目
	 */
	private String goodsCategory;
	/**
	 *  商户操作员编号
	 */
	private String operatorId;
	/**
	 *  卖家支付宝用户ID
	 */
	private String sellerId;
	/**
	 *  卖家支付宝账号
	 */
	private String sellerEmail;
	/**
	 *  商户门店编号
	 */
	private String storeId;
	/**
	 *  pos每次请求唯一标识
	 */
	private String channelId;
	/**
	 *  主平台商家id
	 */
	private String shopId;
	/**
	 *  机具终端编号
	 */
	private String deviceInfo;
	/**
	 *  门店名称
	 */
	private String storeName;
	/**
	 *  支付超时时间
	 */
	private String timeExpire;
	/**
	 *  二维码码串
	 */
	private String qrCode;
	/**
	 *  支付场景
	 */
	private String scene;
	/**
	 *  支付授权码
	 */
	private String authCode;
	/**
	 *  支付超时时间表达式
	 */
	private String timeoutExpress;
	/**
	 *  买家支付宝用户号
	 */
	private String openId;
	/**
	 *  买家支付宝账号
	 */
	private String buyerLogonId;
	/**
	 *  实收金额
	 */
	private java.math.BigDecimal receiptAmount;
	/**
	 *  开票金额
	 */
	private java.math.BigDecimal invoiceAmount;
	/**
	 *  付款金额
	 */
	private java.math.BigDecimal buyerPayAmount;
	/**
	 *  积分宝金额
	 */
	private java.math.BigDecimal pointAmount;
	/**
	 *  交易创建时间
	 */
	private java.util.Date gmtCreate;
	/**
	 *  付款时间
	 */
	private java.util.Date gmtPayment;
	/**
	 *  交易状态
	 */
	private String tradeStatus;
	/**
	 *  交易状态描述
	 */
	private String tradeStatusInfo;
	/**
	 *  合作者身份
	 */
	private String partner;
	/**
	 *  支付宝结果码
	 */
	private String code;
	/**
	 *  支付宝结果码描述
	 */
	private String msg;
	/**
	 *  支付宝错误子代码
	 */
	private String subCode;
	/**
	 *  支付宝错误子代码描述
	 */
	private String subMsg;
	/**
	 *  数据来源0新增2补录
	 */
	private Integer dataSource;
	/**
	 *  退款错误次数
	 */
	private Integer errorTimes;
	/**
	 *  分账信息
	 */
	private String royaltyInfo;
	/**
	 *  订单创建时间
	 */
	private java.util.Date createTime;
	/**
	 *  支付宝红包
	 */
	private java.math.BigDecimal coupon;
	/**
	 *  支付宝余额
	 */
	private java.math.BigDecimal alipayAccount;
	/**
	 *  积分
	 */
	private java.math.BigDecimal point;
	/**
	 *  折扣券
	 */
	private java.math.BigDecimal discount;
	/**
	 *  预付卡
	 */
	private java.math.BigDecimal pcard;
	/**
	 *  商户店铺卡
	 */
	private java.math.BigDecimal mcard;
	/**
	 *  商户优惠券
	 */
	private java.math.BigDecimal mdiscount;
	/**
	 *  商户红包
	 */
	private java.math.BigDecimal mcoupon;
	/**
	 *  蚂蚁花呗
	 */
	private java.math.BigDecimal pcredit;
	/**
	 *  设备通知标识
	 */
	private String deviceInfoPartner;
	/**
	 *  订单业务类型: BARCODE_PAY_OFFLINE：条码支付;SOUNDWAVE_PAY_OFFLINE：声波支付;MEMBER_CARD_QR_OFFLINE：会员卡支付; FUND_TRADE_FAST_PAY：预授权产品     ; FINGERPRINT_FAST_PAY：指纹支付
	 */
	private String productCode;
	/**
	 *  余额宝  string表示使用余额宝支付的金额，单位元。可空
	 */
	private java.math.BigDecimal financeAccount;
	/**
	 *  实收金额，支付宝、余额宝、银行卡三种支付总和
	 */
	private java.math.BigDecimal chargeFee;
	/**
	 *  优惠金额
	 */
	private java.math.BigDecimal preferentialFee;
	/**
	 *  状态更正状态时间
	 */
	private java.util.Date correctTime;
	/**
	 *  门店编码
	 */
	private String storeCode;
	/**
	 *  花呗分期数
	 */
	private String hbfqnum;
	/**
	 *  花呗卖家承担付费比例
	 */
	private String hbfqsellerpercent;
	/**
	 * 
	 * @param id
	 */
	public void setId(Integer id){
		this.id = id;
	}
	
    /**
     * 
     * @return
     */	
    public Integer getId(){
    	return id;
    }
	/**
	 * 用户id
	 * @param userId
	 */
	public void setUserId(Integer userId){
		this.userId = userId;
	}
	
    /**
     * 用户id
     * @return
     */	
    public Integer getUserId(){
    	return userId;
    }
	/**
	 * 支付宝接口名称
	 * @param method
	 */
	public void setMethod(String method){
		this.method = method;
	}
	
    /**
     * 支付宝接口名称
     * @return
     */	
    public String getMethod(){
    	return method;
    }
	/**
	 * 支付宝交易凭证号
	 * @param tradeNo
	 */
	public void setTradeNo(String tradeNo){
		this.tradeNo = tradeNo;
	}
	
    /**
     * 支付宝交易凭证号
     * @return
     */	
    public String getTradeNo(){
    	return tradeNo;
    }
	/**
	 * 商户订单号
	 * @param outTradeNo
	 */
	public void setOutTradeNo(String outTradeNo){
		this.outTradeNo = outTradeNo;
	}
	
    /**
     * 商户订单号
     * @return
     */	
    public String getOutTradeNo(){
    	return outTradeNo;
    }
	/**
	 * 订单金额
	 * @param totalAmount
	 */
	public void setTotalAmount(java.math.BigDecimal totalAmount){
		this.totalAmount = totalAmount;
	}
	
    /**
     * 订单金额
     * @return
     */	
    public java.math.BigDecimal getTotalAmount(){
    	return totalAmount;
    }
	/**
	 * 可打折金额
	 * @param discountableAmount
	 */
	public void setDiscountableAmount(java.math.BigDecimal discountableAmount){
		this.discountableAmount = discountableAmount;
	}
	
    /**
     * 可打折金额
     * @return
     */	
    public java.math.BigDecimal getDiscountableAmount(){
    	return discountableAmount;
    }
	/**
	 * 不可打折金额
	 * @param undiscountableAmount
	 */
	public void setUndiscountableAmount(java.math.BigDecimal undiscountableAmount){
		this.undiscountableAmount = undiscountableAmount;
	}
	
    /**
     * 不可打折金额
     * @return
     */	
    public java.math.BigDecimal getUndiscountableAmount(){
    	return undiscountableAmount;
    }
	/**
	 * 订单标题
	 * @param subject
	 */
	public void setSubject(String subject){
		this.subject = subject;
	}
	
    /**
     * 订单标题
     * @return
     */	
    public String getSubject(){
    	return subject;
    }
	/**
	 * 订单描述
	 * @param body
	 */
	public void setBody(String body){
		this.body = body;
	}
	
    /**
     * 订单描述
     * @return
     */	
    public String getBody(){
    	return body;
    }
	/**
	 * 商品编号
	 * @param goodsId
	 */
	public void setGoodsId(String goodsId){
		this.goodsId = goodsId;
	}
	
    /**
     * 商品编号
     * @return
     */	
    public String getGoodsId(){
    	return goodsId;
    }
	/**
	 * 支付宝统一的商品编号
	 * @param alipayGoodsId
	 */
	public void setAlipayGoodsId(String alipayGoodsId){
		this.alipayGoodsId = alipayGoodsId;
	}
	
    /**
     * 支付宝统一的商品编号
     * @return
     */	
    public String getAlipayGoodsId(){
    	return alipayGoodsId;
    }
	/**
	 * 商品名称
	 * @param goodsName
	 */
	public void setGoodsName(String goodsName){
		this.goodsName = goodsName;
	}
	
    /**
     * 商品名称
     * @return
     */	
    public String getGoodsName(){
    	return goodsName;
    }
	/**
	 * 商品数量
	 * @param quantity
	 */
	public void setQuantity(String quantity){
		this.quantity = quantity;
	}
	
    /**
     * 商品数量
     * @return
     */	
    public String getQuantity(){
    	return quantity;
    }
	/**
	 * 商品单价
	 * @param price
	 */
	public void setPrice(java.math.BigDecimal price){
		this.price = price;
	}
	
    /**
     * 商品单价
     * @return
     */	
    public java.math.BigDecimal getPrice(){
    	return price;
    }
	/**
	 * 商品类目
	 * @param goodsCategory
	 */
	public void setGoodsCategory(String goodsCategory){
		this.goodsCategory = goodsCategory;
	}
	
    /**
     * 商品类目
     * @return
     */	
    public String getGoodsCategory(){
    	return goodsCategory;
    }
	/**
	 * 商户操作员编号
	 * @param operatorId
	 */
	public void setOperatorId(String operatorId){
		this.operatorId = operatorId;
	}
	
    /**
     * 商户操作员编号
     * @return
     */	
    public String getOperatorId(){
    	return operatorId;
    }
	/**
	 * 卖家支付宝用户ID
	 * @param sellerId
	 */
	public void setSellerId(String sellerId){
		this.sellerId = sellerId;
	}
	
    /**
     * 卖家支付宝用户ID
     * @return
     */	
    public String getSellerId(){
    	return sellerId;
    }
	/**
	 * 卖家支付宝账号
	 * @param sellerEmail
	 */
	public void setSellerEmail(String sellerEmail){
		this.sellerEmail = sellerEmail;
	}
	
    /**
     * 卖家支付宝账号
     * @return
     */	
    public String getSellerEmail(){
    	return sellerEmail;
    }
	/**
	 * 商户门店编号
	 * @param storeId
	 */
	public void setStoreId(String storeId){
		this.storeId = storeId;
	}
	
    /**
     * 商户门店编号
     * @return
     */	
    public String getStoreId(){
    	return storeId;
    }
	/**
	 * pos每次请求唯一标识
	 * @param channelId
	 */
	public void setChannelId(String channelId){
		this.channelId = channelId;
	}
	
    /**
     * pos每次请求唯一标识
     * @return
     */	
    public String getChannelId(){
    	return channelId;
    }
	/**
	 * 主平台商家id
	 * @param shopId
	 */
	public void setShopId(String shopId){
		this.shopId = shopId;
	}
	
    /**
     * 主平台商家id
     * @return
     */	
    public String getShopId(){
    	return shopId;
    }
	/**
	 * 机具终端编号
	 * @param deviceInfo
	 */
	public void setDeviceInfo(String deviceInfo){
		this.deviceInfo = deviceInfo;
	}
	
    /**
     * 机具终端编号
     * @return
     */	
    public String getDeviceInfo(){
    	return deviceInfo;
    }
	/**
	 * 门店名称
	 * @param storeName
	 */
	public void setStoreName(String storeName){
		this.storeName = storeName;
	}
	
    /**
     * 门店名称
     * @return
     */	
    public String getStoreName(){
    	return storeName;
    }
	/**
	 * 支付超时时间
	 * @param timeExpire
	 */
	public void setTimeExpire(String timeExpire){
		this.timeExpire = timeExpire;
	}
	
    /**
     * 支付超时时间
     * @return
     */	
    public String getTimeExpire(){
    	return timeExpire;
    }
	/**
	 * 二维码码串
	 * @param qrCode
	 */
	public void setQrCode(String qrCode){
		this.qrCode = qrCode;
	}
	
    /**
     * 二维码码串
     * @return
     */	
    public String getQrCode(){
    	return qrCode;
    }
	/**
	 * 支付场景
	 * @param scene
	 */
	public void setScene(String scene){
		this.scene = scene;
	}
	
    /**
     * 支付场景
     * @return
     */	
    public String getScene(){
    	return scene;
    }
	/**
	 * 支付授权码
	 * @param authCode
	 */
	public void setAuthCode(String authCode){
		this.authCode = authCode;
	}
	
    /**
     * 支付授权码
     * @return
     */	
    public String getAuthCode(){
    	return authCode;
    }
	/**
	 * 支付超时时间表达式
	 * @param timeoutExpress
	 */
	public void setTimeoutExpress(String timeoutExpress){
		this.timeoutExpress = timeoutExpress;
	}
	
    /**
     * 支付超时时间表达式
     * @return
     */	
    public String getTimeoutExpress(){
    	return timeoutExpress;
    }
	/**
	 * 买家支付宝用户号
	 * @param openId
	 */
	public void setOpenId(String openId){
		this.openId = openId;
	}
	
    /**
     * 买家支付宝用户号
     * @return
     */	
    public String getOpenId(){
    	return openId;
    }
	/**
	 * 买家支付宝账号
	 * @param buyerLogonId
	 */
	public void setBuyerLogonId(String buyerLogonId){
		this.buyerLogonId = buyerLogonId;
	}
	
    /**
     * 买家支付宝账号
     * @return
     */	
    public String getBuyerLogonId(){
    	return buyerLogonId;
    }
	/**
	 * 实收金额
	 * @param receiptAmount
	 */
	public void setReceiptAmount(java.math.BigDecimal receiptAmount){
		this.receiptAmount = receiptAmount;
	}
	
    /**
     * 实收金额
     * @return
     */	
    public java.math.BigDecimal getReceiptAmount(){
    	return receiptAmount;
    }
	/**
	 * 开票金额
	 * @param invoiceAmount
	 */
	public void setInvoiceAmount(java.math.BigDecimal invoiceAmount){
		this.invoiceAmount = invoiceAmount;
	}
	
    /**
     * 开票金额
     * @return
     */	
    public java.math.BigDecimal getInvoiceAmount(){
    	return invoiceAmount;
    }
	/**
	 * 付款金额
	 * @param buyerPayAmount
	 */
	public void setBuyerPayAmount(java.math.BigDecimal buyerPayAmount){
		this.buyerPayAmount = buyerPayAmount;
	}
	
    /**
     * 付款金额
     * @return
     */	
    public java.math.BigDecimal getBuyerPayAmount(){
    	return buyerPayAmount;
    }
	/**
	 * 积分宝金额
	 * @param pointAmount
	 */
	public void setPointAmount(java.math.BigDecimal pointAmount){
		this.pointAmount = pointAmount;
	}
	
    /**
     * 积分宝金额
     * @return
     */	
    public java.math.BigDecimal getPointAmount(){
    	return pointAmount;
    }
	/**
	 * 交易创建时间
	 * @param gmtCreate
	 */
	public void setGmtCreate(java.util.Date gmtCreate){
		this.gmtCreate = gmtCreate;
	}
	
    /**
     * 交易创建时间
     * @return
     */	
    public java.util.Date getGmtCreate(){
    	return gmtCreate;
    }
	/**
	 * 付款时间
	 * @param gmtPayment
	 */
	public void setGmtPayment(java.util.Date gmtPayment){
		this.gmtPayment = gmtPayment;
	}
	
    /**
     * 付款时间
     * @return
     */	
    public java.util.Date getGmtPayment(){
    	return gmtPayment;
    }
	/**
	 * 交易状态
	 * @param tradeStatus
	 */
	public void setTradeStatus(String tradeStatus){
		this.tradeStatus = tradeStatus;
	}
	
    /**
     * 交易状态
     * @return
     */	
    public String getTradeStatus(){
    	return tradeStatus;
    }
	/**
	 * 交易状态描述
	 * @param tradeStatusInfo
	 */
	public void setTradeStatusInfo(String tradeStatusInfo){
		this.tradeStatusInfo = tradeStatusInfo;
	}
	
    /**
     * 交易状态描述
     * @return
     */	
    public String getTradeStatusInfo(){
    	return tradeStatusInfo;
    }
	/**
	 * 合作者身份
	 * @param partner
	 */
	public void setPartner(String partner){
		this.partner = partner;
	}
	
    /**
     * 合作者身份
     * @return
     */	
    public String getPartner(){
    	return partner;
    }
	/**
	 * 支付宝结果码
	 * @param code
	 */
	public void setCode(String code){
		this.code = code;
	}
	
    /**
     * 支付宝结果码
     * @return
     */	
    public String getCode(){
    	return code;
    }
	/**
	 * 支付宝结果码描述
	 * @param msg
	 */
	public void setMsg(String msg){
		this.msg = msg;
	}
	
    /**
     * 支付宝结果码描述
     * @return
     */	
    public String getMsg(){
    	return msg;
    }
	/**
	 * 支付宝错误子代码
	 * @param subCode
	 */
	public void setSubCode(String subCode){
		this.subCode = subCode;
	}
	
    /**
     * 支付宝错误子代码
     * @return
     */	
    public String getSubCode(){
    	return subCode;
    }
	/**
	 * 支付宝错误子代码描述
	 * @param subMsg
	 */
	public void setSubMsg(String subMsg){
		this.subMsg = subMsg;
	}
	
    /**
     * 支付宝错误子代码描述
     * @return
     */	
    public String getSubMsg(){
    	return subMsg;
    }
	/**
	 * 数据来源0新增2补录
	 * @param dataSource
	 */
	public void setDataSource(Integer dataSource){
		this.dataSource = dataSource;
	}
	
    /**
     * 数据来源0新增2补录
     * @return
     */	
    public Integer getDataSource(){
    	return dataSource;
    }
	/**
	 * 退款错误次数
	 * @param errorTimes
	 */
	public void setErrorTimes(Integer errorTimes){
		this.errorTimes = errorTimes;
	}
	
    /**
     * 退款错误次数
     * @return
     */	
    public Integer getErrorTimes(){
    	return errorTimes;
    }
	/**
	 * 分账信息
	 * @param royaltyInfo
	 */
	public void setRoyaltyInfo(String royaltyInfo){
		this.royaltyInfo = royaltyInfo;
	}
	
    /**
     * 分账信息
     * @return
     */	
    public String getRoyaltyInfo(){
    	return royaltyInfo;
    }
	/**
	 * 订单创建时间
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime){
		this.createTime = createTime;
	}
	
    /**
     * 订单创建时间
     * @return
     */	
    public java.util.Date getCreateTime(){
    	return createTime;
    }
	/**
	 * 支付宝红包
	 * @param coupon
	 */
	public void setCoupon(java.math.BigDecimal coupon){
		this.coupon = coupon;
	}
	
    /**
     * 支付宝红包
     * @return
     */	
    public java.math.BigDecimal getCoupon(){
    	return coupon;
    }
	/**
	 * 支付宝余额
	 * @param alipayAccount
	 */
	public void setAlipayAccount(java.math.BigDecimal alipayAccount){
		this.alipayAccount = alipayAccount;
	}
	
    /**
     * 支付宝余额
     * @return
     */	
    public java.math.BigDecimal getAlipayAccount(){
    	return alipayAccount;
    }
	/**
	 * 积分
	 * @param point
	 */
	public void setPoint(java.math.BigDecimal point){
		this.point = point;
	}
	
    /**
     * 积分
     * @return
     */	
    public java.math.BigDecimal getPoint(){
    	return point;
    }
	/**
	 * 折扣券
	 * @param discount
	 */
	public void setDiscount(java.math.BigDecimal discount){
		this.discount = discount;
	}
	
    /**
     * 折扣券
     * @return
     */	
    public java.math.BigDecimal getDiscount(){
    	return discount;
    }
	/**
	 * 预付卡
	 * @param pcard
	 */
	public void setPcard(java.math.BigDecimal pcard){
		this.pcard = pcard;
	}
	
    /**
     * 预付卡
     * @return
     */	
    public java.math.BigDecimal getPcard(){
    	return pcard;
    }
	/**
	 * 商户店铺卡
	 * @param mcard
	 */
	public void setMcard(java.math.BigDecimal mcard){
		this.mcard = mcard;
	}
	
    /**
     * 商户店铺卡
     * @return
     */	
    public java.math.BigDecimal getMcard(){
    	return mcard;
    }
	/**
	 * 商户优惠券
	 * @param mdiscount
	 */
	public void setMdiscount(java.math.BigDecimal mdiscount){
		this.mdiscount = mdiscount;
	}
	
    /**
     * 商户优惠券
     * @return
     */	
    public java.math.BigDecimal getMdiscount(){
    	return mdiscount;
    }
	/**
	 * 商户红包
	 * @param mcoupon
	 */
	public void setMcoupon(java.math.BigDecimal mcoupon){
		this.mcoupon = mcoupon;
	}
	
    /**
     * 商户红包
     * @return
     */	
    public java.math.BigDecimal getMcoupon(){
    	return mcoupon;
    }
	/**
	 * 蚂蚁花呗
	 * @param pcredit
	 */
	public void setPcredit(java.math.BigDecimal pcredit){
		this.pcredit = pcredit;
	}
	
    /**
     * 蚂蚁花呗
     * @return
     */	
    public java.math.BigDecimal getPcredit(){
    	return pcredit;
    }
	/**
	 * 设备通知标识
	 * @param deviceInfoPartner
	 */
	public void setDeviceInfoPartner(String deviceInfoPartner){
		this.deviceInfoPartner = deviceInfoPartner;
	}
	
    /**
     * 设备通知标识
     * @return
     */	
    public String getDeviceInfoPartner(){
    	return deviceInfoPartner;
    }
	/**
	 * 订单业务类型: BARCODE_PAY_OFFLINE：条码支付;SOUNDWAVE_PAY_OFFLINE：声波支付;MEMBER_CARD_QR_OFFLINE：会员卡支付; FUND_TRADE_FAST_PAY：预授权产品     ; FINGERPRINT_FAST_PAY：指纹支付
	 * @param productCode
	 */
	public void setProductCode(String productCode){
		this.productCode = productCode;
	}
	
    /**
     * 订单业务类型: BARCODE_PAY_OFFLINE：条码支付;SOUNDWAVE_PAY_OFFLINE：声波支付;MEMBER_CARD_QR_OFFLINE：会员卡支付; FUND_TRADE_FAST_PAY：预授权产品     ; FINGERPRINT_FAST_PAY：指纹支付
     * @return
     */	
    public String getProductCode(){
    	return productCode;
    }
	/**
	 * 余额宝  string表示使用余额宝支付的金额，单位元。可空
	 * @param financeAccount
	 */
	public void setFinanceAccount(java.math.BigDecimal financeAccount){
		this.financeAccount = financeAccount;
	}
	
    /**
     * 余额宝  string表示使用余额宝支付的金额，单位元。可空
     * @return
     */	
    public java.math.BigDecimal getFinanceAccount(){
    	return financeAccount;
    }
	/**
	 * 实收金额，支付宝、余额宝、银行卡三种支付总和
	 * @param chargeFee
	 */
	public void setChargeFee(java.math.BigDecimal chargeFee){
		this.chargeFee = chargeFee;
	}
	
    /**
     * 实收金额，支付宝、余额宝、银行卡三种支付总和
     * @return
     */	
    public java.math.BigDecimal getChargeFee(){
    	return chargeFee;
    }
	/**
	 * 优惠金额
	 * @param preferentialFee
	 */
	public void setPreferentialFee(java.math.BigDecimal preferentialFee){
		this.preferentialFee = preferentialFee;
	}
	
    /**
     * 优惠金额
     * @return
     */	
    public java.math.BigDecimal getPreferentialFee(){
    	return preferentialFee;
    }
	/**
	 * 状态更正状态时间
	 * @param correctTime
	 */
	public void setCorrectTime(java.util.Date correctTime){
		this.correctTime = correctTime;
	}
	
    /**
     * 状态更正状态时间
     * @return
     */	
    public java.util.Date getCorrectTime(){
    	return correctTime;
    }
	/**
	 * 门店编码
	 * @param storeCode
	 */
	public void setStoreCode(String storeCode){
		this.storeCode = storeCode;
	}
	
    /**
     * 门店编码
     * @return
     */	
    public String getStoreCode(){
    	return storeCode;
    }
	/**
	 * 花呗分期数
	 * @param hbfqnum
	 */
	public void setHbfqnum(String hbfqnum){
		this.hbfqnum = hbfqnum;
	}
	
    /**
     * 花呗分期数
     * @return
     */	
    public String getHbfqnum(){
    	return hbfqnum;
    }
	/**
	 * 花呗卖家承担付费比例
	 * @param hbfqsellerpercent
	 */
	public void setHbfqsellerpercent(String hbfqsellerpercent){
		this.hbfqsellerpercent = hbfqsellerpercent;
	}
	
    /**
     * 花呗卖家承担付费比例
     * @return
     */	
    public String getHbfqsellerpercent(){
    	return hbfqsellerpercent;
    }
}