package com.appointment.pay.core.mapper;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;
import com.appointment.pay.core.entity.OrderInfo;
import com.hyxt.platform.commons.page.Page;
import com.hyxt.platform.commons.page.Pageable;

public interface OrderInfoMapper {

	void insertOrderInfo(OrderInfo orderInfo);

	void deleteOrderInfoById(Integer id);

	void updateOrderInfo(OrderInfo orderInfo);

	Page<OrderInfo> searchOrderInfoByParams(@Param("map") Map<String, Object> map , Pageable pageable);

	List<OrderInfo> searchOrderInfoByParams(@Param("map") Map<String, Object> map);
	
	OrderInfo queryByOutTradeNo(@Param("outTradeNo") String outTradeNo);

} 
