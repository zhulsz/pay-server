package com.appointment.pay.core.entity;

import java.io.Serializable;

/**
 * 微信异步通知对象
 * @author zhul
 *
 */
public class WxpayNotify implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -3128048547287717657L;
    
    /**
     * 返回状态码,SUCCESS/FAIL 
     */
    private String return_code;
    
    /**
     * 返回信息
     */
    private String return_msg;
    
    /**
     * 应用ID
     */
    private String appid;
    
    /**
     * 商户号
     */
    private String mch_id;
    
    /**
     * 设备号
     */
    private String device_info;
    
    /**
     * 随机字符串
     */
    private String nonce_str;
    
    /**
     * 签名
     */
    private String sign;
    
    /**
     * 业务结果,SUCCESS/FAIL
     */
    private String result_code;
    
   /**
    * 错误代码
    */
    private String err_code;
    
    /**
     * 错误代码描述
     */
    private String err_code_des;
    
    /**
     * 用户标识
     */
    private String openid;
    
    /**
     * 是否关注公众账号
     */
    private String is_subscribe;
    
    /**
     * 调用接口提交的交易类型，取值如下：JSAPI，NATIVE，APP，
     */
    private String trade_type;
    
    /**
     * 付款银行
     */
    private String bank_type;
    
    /**
     * 总金额
     */
    private Integer total_fee;
    
    /**
     * 货币种类
     */
    private String fee_type;
    
    /**
     * 现金支付金额
     */
    private Integer cash_fee;
    
    /**
     * 现金支付货币类型
     */
    private String cash_fee_type;
    
    /**
     * 代金券或立减优惠金额
     */
    private Integer coupon_fee;
    
    /**
     * 代金券或立减优惠使用数量
     */
    private Integer coupon_count;
    
    /**
     * 微信支付订单号
     */
    private String transaction_id;
    
    /**
     * 商户订单号
     */
    private String out_trade_no;
    
    /**
     * 商家数据包
     */
    private String attach;
    
    /**
     * 支付完成时间,格式为yyyyMMddHHmmss
     */
    private String time_end;
    
}
