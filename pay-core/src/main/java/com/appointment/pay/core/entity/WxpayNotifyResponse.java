package com.appointment.pay.core.entity;

import java.io.Serializable;

/**
 * 异步通知响应信息
 * @author zhul
 *
 */
public class WxpayNotifyResponse implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 6839597346757425121L;
    
    /**
     * 返回状态码,SUCCESS/FAIL 
     */
    private String return_code;
    
    /**
     * 返回信息
     */
    private String return_msg;

    public String getReturn_code() {
        return return_code;
    }

    public void setReturn_code(String return_code) {
        this.return_code = return_code;
    }

    public String getReturn_msg() {
        return return_msg;
    }

    public void setReturn_msg(String return_msg) {
        this.return_msg = return_msg;
    }
    
}
