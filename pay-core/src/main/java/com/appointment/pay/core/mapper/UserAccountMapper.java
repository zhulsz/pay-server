package com.appointment.pay.core.mapper;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;
import com.appointment.pay.core.entity.UserAccount;
import com.hyxt.platform.commons.page.Page;
import com.hyxt.platform.commons.page.Pageable;

public interface UserAccountMapper {

	void insertUserAccount(UserAccount userAccount);

	void deleteUserAccountById(Integer id);

	void updateUserAccount(UserAccount userAccount);

	Page<UserAccount> searchUserAccountByParams(@Param("map")Map<String, String> map , Pageable pageable);

	List<UserAccount> searchUserAccountByParams(@Param("map")Map<String, String> map);
	
	UserAccount queryByUserId(@Param("userId") Integer userId);

} 
