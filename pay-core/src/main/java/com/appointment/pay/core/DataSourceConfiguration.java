package com.appointment.pay.core;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

@Configuration
@MapperScan(basePackages = "com.appointment.pay.core.mapper")
@PropertySource("classpath:datasource.properties")
public class DataSourceConfiguration {
}
