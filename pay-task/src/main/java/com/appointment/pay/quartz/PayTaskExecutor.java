package com.appointment.pay.quartz;

import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.appointment.pay.core.entity.WxPayTransactionHistory;
import com.appointment.pay.core.enums.TradeStateEnum;
import com.appointment.pay.core.mapper.WxPayTransactionHistoryMapper;
import com.appointment.pay.service.IWxPayTransactionService;

/**
 * 支付定时任务
 * @author zhul
 *
 */
@Component
@SuppressWarnings({ "rawtypes" })
public class PayTaskExecutor {
    
    private static Logger logger = LoggerFactory.getLogger(PayTaskExecutor.class);
    
    @Autowired
    private WxPayTransactionHistoryMapper wxPayTransactionHistoryMapper;
    
    @Autowired
    private IWxPayTransactionService wxPayTransactionService;
        
    /**
     * 每5秒查询微信未支付订单
     */
    @Scheduled(fixedDelay = 5000)
    public void handleWxUnPayment() {
        logger.info("定时查询微信未支付单开始执行...");
        long beginTime = new Date().getTime();
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("tradeState", TradeStateEnum.NOTPAY.getCode());//未支付
        Calendar rightNow = Calendar.getInstance();
        Date endDate = rightNow.getTime();//查询结束时间
        rightNow.add(Calendar.MINUTE, -5);
        Date beginDate = rightNow.getTime();//查询开始时间
        map.put("beginDate", beginDate);
        map.put("endDate", endDate);
        List<WxPayTransactionHistory> tranList = wxPayTransactionHistoryMapper.searchWxPayTransactionHistoryByParams(map);
        if(tranList != null && tranList.size() > 0){
            logger.info("未支付的微信订单数为："+tranList.size());
            for(WxPayTransactionHistory tran : tranList){
                String appid = tran.getAppId();
                String mch_id = tran.getMchId();
                String out_trade_no = tran.getOutTradeNo();
                wxPayTransactionService.queryWxpayOrder(appid, mch_id, out_trade_no);
            }
        }else{
            logger.info("没有未支付的微信订单。");
        }

        long endTime = new Date().getTime();
        logger.info("定时查询微信未支付单执行完成. 用时==="+(endTime-beginTime)/1000+"秒...");
    }
    
    /**
     * 关闭5分钟之前的未支付订单
     */
    @Scheduled(fixedDelay = 60000)
    public void closeWxUnPayment() {
        logger.info("定时关闭微信未支付单开始执行...");
        long beginTime = new Date().getTime();
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("tradeState", TradeStateEnum.NOTPAY.getCode());//未支付
        Calendar rightNow = Calendar.getInstance();
        rightNow.add(Calendar.MINUTE, -5);
        Date endDate = rightNow.getTime();//查询5分钟之前的订单
        map.put("endDate", endDate);
        List<WxPayTransactionHistory> tranList = wxPayTransactionHistoryMapper.searchWxPayTransactionHistoryByParams(map);
        if(tranList != null && tranList.size() > 0){
            logger.info("待关闭的微信订单数为："+tranList.size());
            for(WxPayTransactionHistory tran : tranList){
                String appid = tran.getAppId();
                String mch_id = tran.getMchId();
                String out_trade_no = tran.getOutTradeNo();
                wxPayTransactionService.closeWxpayOrder(appid, mch_id, out_trade_no);
            }
        }else{
            logger.info("没有待关闭的微信订单。");
        }

        long endTime = new Date().getTime();
        logger.info("定时关闭微信未支付单执行完成. 用时==="+(endTime-beginTime)/1000+"秒...");
    }
}
