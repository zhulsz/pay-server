package com.appointment.pay.service;


/**
 * 微信支付业务接口
 * @author zhul
 *
 */
public interface IWxPayTransactionService {
    
    /**
     * 微信订单查询接口查询订单
     * @param appid
     * @param mch_id
     * @param out_trade_no
     */
    void queryWxpayOrder(String appid,String mch_id,String out_trade_no);
    
    /**
     * 关闭未支付订单
     * @param appid
     * @param mch_id
     * @param out_trade_no
     */
    void closeWxpayOrder(String appid,String mch_id,String out_trade_no);
    
}
