package com.appointment.pay.util;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * 日期处理工具类
 * @author zhul
 *
 */
public class DateUtils {

	public static String FORMAT = "yyyy-MM-dd HH:mm:ss";
	
	public static String FORMAT2 = "yyyyMMddHHmmss";
	
	/**
	 * 获取过期时间
	 * @param liveTime 存活时间
	 * @return 单位：秒
	 */
	public static long getExpTime(int liveTime){
		 Calendar rightNow = Calendar.getInstance();
		 rightNow.add(Calendar.SECOND, liveTime);
		 return rightNow.getTimeInMillis();
	}
	
	/**
	 * 获取当前时间，格式为：yyyyMMddHHmmss
	 * @return
	 */
	public static String printNow(){
	    Calendar rightNow = Calendar.getInstance();
	    SimpleDateFormat format = new SimpleDateFormat(FORMAT2);
	    return format.format(rightNow.getTime());
	}
	
	
	public static void main(String[] args) {
		/*SimpleDateFormat format = new SimpleDateFormat(FORMAT);
		long t = getExpTime(3600);
		System.out.println("过期时间是："+format.format(new Date(t)));*/

	    System.out.println("当前时间=="+printNow());
	    
	    String out_trade_no = SignUtils.createOutTradeNo();
	    System.out.println("订单号=="+out_trade_no);
        String nonce_str = MD5Util.MD5Encode(out_trade_no, "utf-8");
        System.out.println("随机串=="+nonce_str);
        
        String ip = SignUtils.getLocalIP();
        System.out.println("ip=="+ip);
	}

}
