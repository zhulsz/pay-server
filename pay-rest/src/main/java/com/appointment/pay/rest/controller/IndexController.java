package com.appointment.pay.rest.controller;

import javax.servlet.http.HttpServletResponse;

import net.sf.json.JSONObject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * @author zhul
 *
 */
@Controller
@RequestMapping("/")
public class IndexController {
    
    private static Logger logger = LoggerFactory.getLogger(IndexController.class);
    
    @Autowired
    private StringRedisTemplate redisTemplate;
   
    /**
     * 处理终端接口
     * 
     * @param bodyStr
     * @return
     */
    @RequestMapping(method=RequestMethod.GET)
    public @ResponseBody String index(HttpServletResponse response) {
        
        JSONObject resultJson = new JSONObject();
        resultJson.put("stateCode", "0000");
        resultJson.put("stateMessage", "系统已启动...");       
        response.setContentType("text/html;charset=UTF-8");
        
        return resultJson.toString();
    }    
    
}
