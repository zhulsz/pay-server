package com.appointment.pay.rest.service;

import java.util.SortedMap;

/**
 * 微信支付业务接口
 * @author zhul
 *
 */
public interface IWxPayTransactionService {
    
    /**
     * 处理微信异步通知数据
     * @param parameters
     */
    void handlePayNotify(SortedMap<Object,Object> parameters);
    
}
