package com.appointment.pay.rest.exception;

/**
 * Created by zhul on 2016/5/5.
 */
public class PayException extends RuntimeException {
    /**
     * 
     */
    private static final long serialVersionUID = 5544359914842381987L;
    
    private String errorCode = "";

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public PayException(String message, String errorCode) {
        super(errorCode+message);
        this.errorCode = errorCode;
    }

    public PayException(String message, Throwable cause, String errorCode) {
        super(message, cause);
        this.errorCode = errorCode;
    }

    public PayException(Throwable cause, String errorCode) {
        super(cause);
        this.errorCode = errorCode;
    }

    public PayException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace, String errorCode) {
        //super(message, cause, enableSuppression, writableStackTrace);
        this.errorCode = errorCode;
    }

    public PayException(String message) {
        super(message);
    }

    public PayException(String message, Throwable cause) {
        super(message, cause);
    }

    public PayException(Throwable cause) {
        super(cause);
    }

    public PayException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
       // super(message, cause, enableSuppression, writableStackTrace);
    }
}
