package com.appointment.pay.rest.enums;

/**
 * Created by rocky on 14-9-26.
 */
public enum ErrEnum {

    ERR01("1000" , "appid为空。") ,
    
    ERR02("1001" , "商品描述为空。") ,
    
    ERR03("1002" , "商户号为空。") ,
    
    ERR04("1003" , "金额不正确。") ,
    
    ERR05("1004" , "交易类型为空。") ,
    
    ERR06("1005" , "密钥为空。") ,
    
    ERR07("2000" , "调用微信支付下单接口异常!") ,
    
    ERR08("1006" , "userId 为空。") ,
    
    ERR09("2001" , "调用微信支付查单接口异常!") ,
    
    ERR10("3000" , "参数biz_content为空。") ,
    
    ERR11("3001" , "参数app_id为空。") ,
    
    ERR100("5000" , "系统内部错误！") ;

    private String code ;

    private String msg ;

    ErrEnum(String code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    public String getCode() {
        return code;
    }

    public static String getMsg(int code) {
        for (ErrEnum errEnum : values()) {
            int index = errEnum.getCode().indexOf("-");
            if (Integer.valueOf(errEnum.getCode().substring(index, index+2)) == code) {
                return errEnum.originMsg();
            }
        }
        return null;
    }

    public String originMsg() {
        return msg;
    }

    public String getMsg() {
        return msg;
    }
}
