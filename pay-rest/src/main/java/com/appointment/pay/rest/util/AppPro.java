package com.appointment.pay.rest.util;

import java.io.InputStream;
import java.util.Properties;

public class AppPro {
	private static Properties prop = null;
	private static boolean isPropLoaded = false;

	public AppPro() {

	}

	public static String getProp(String key) {
		load();

		return prop.getProperty(key);
	}

	private static void load() {
		if (!isPropLoaded) {
			try {
				prop = new Properties();
				InputStream in = new AppPro().getClass().getResourceAsStream("/config.properties");
				prop.load(in);
				isPropLoaded = true;
			} catch (Exception e) {
				System.err.println("读取config.properties异常：" + e.toString());
			}
		}
	}
}
